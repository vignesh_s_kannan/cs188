
import util
from models import *


def SolveCSPProblem(problem, depth):
    i = problem.getMRV()
    if i is None:
        return True

    v = problem.getVariable(i)
    while v.getRemainingValues() != 0:

        j = v.getUnScratchedValueIndex()
        v.scratchIndexAsSeen(j, depth)
        v.assignValue(j)

        empty = problem.forwardChecking(i, j, depth)
        if empty:
            v.unAssignValue()
            problem.undoScratchAtDepth(depth, "ForwardChecking")
            continue
        result = SolveCSPProblem(problem, depth+1)
        if result:
            return True
        else:
            v.unAssignValue()
            problem.undoScratchAtDepth(depth, "ForwardChecking")

    problem.undoScratchAtDepth(depth, "Seen")
    return False



if __name__ == "__main__":
    a = Sudoku()
    SolveCSPProblem(a, 1)
    print a
    # print a.domainList()
