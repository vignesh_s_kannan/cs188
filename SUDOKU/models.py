
import random

class CSPProblem:
    """
    This class outlines the structure of a CSP problem, but doesn't implement
    any of the methods (in object-oriented terminology: an abstract class).
    """
    def makeArcConsistent(self, depth):
        util.raiseNotDefined()

    def getMRV(self):
        util.raiseNotDefined()

    def getLCV(self, mrv):
        util.raiseNotDefined()


class Variable:

    def __init__(self, domain):
        self.domain = domain
        self.scratched = [None for i in range(len(domain))]
        self.value = None

    def getRemainingValues(self):
        if self.value is None:
            return sum(x is None for x in self.scratched)
        else:
            return None

    def getUnScratchedValueIndex(self):
        candidates = []
        for i, val in enumerate(self.scratched):
            if val is None:
                candidates.append(i)
        if len(candidates) == 0:
            return None
        else:
            return random.choice(candidates)

    def scratchIndexAsSeen(self, index, depth):
        self.scratched[index] = {"type":"Seen", "value":depth}

    def undoScratchAtDepth(self, depth, val):
        for i, detail in enumerate(self.scratched):
            if (detail is not None):
                if  (detail["type"] == val) & (detail["value"] == depth):
                    self.scratched[i] = None

    def assignValue(self, value):
        self.value = self.domain[value]

    def unAssignValue(self):
        self.value = None

    def __str__(self):
        form = ""
        for i,v in enumerate(self.scratched):
            form = form + str(self.domain[i]) + "-" +str(v)+" "
        return " value " + str(self.value) + " " + form


class Sudoku(CSPProblem):

    def __init__(self):
        self.rowBound = 9
        self.columnBound = 9
        self.variables = [[Variable([i for i in range(1,10)]) for i in range(self.columnBound)] for i in range(self.rowBound)]
        self.MRV = [0,0]

    def forwardChecking(self, i, j, depth):
        # Column Checking
        root = self.getVariable(i)

        columnVars = []
        for col in range(self.columnBound):
            if col != i[1]:
                columnVars.append(self.variables[i[0]][col])

        result1 = self.listOfNeighbors(root, columnVars, depth)
        if result1:
            return True

        rowVars = []
        for row in range(self.rowBound):
            if row != i[0]:
                rowVars.append(self.variables[row][i[1]])

        result2 = self.listOfNeighbors(root, rowVars, depth)
        if result2:
            return True

        boxVars = self.findBoxNeighbors(i)
        result2 = self.listOfNeighbors(root, boxVars, depth)
        if result2:
            return True
            
        return False

    def findLimit(self, i, side):
        if side == "plus":
            j = i
            limit = self.rowBound
            while j<= limit:
                if j%3 ==0:
                    return j-1
                j = j+1
        else:
            j = i-1
            limit = 0
            while j>= limit:
                if j%3 ==0:
                    return j
                j = j-1

    def findBoxNeighbors(self, i):

        horizontalA = self.findLimit(i[0]+1, "minus")
        horizontalB = self.findLimit(i[0]+1, "plus")
        verticalA = self.findLimit(i[1]+1, "minus")
        verticalB = self.findLimit(i[1]+1, "plus")

        boxvars = []
        for row in range(horizontalA, horizontalB+1):
            for col in range(verticalA, verticalB+1):
                if (row != i[0]) & (col != i[1]):
                    boxvars.append(self.variables[row][col])
        return boxvars

    def listOfNeighbors(self, root, listofVars, depth):
        for item in listofVars:
            if item.value == root.value:
                return True
            for index,val in enumerate(item.scratched):
                if (val is None) & (item.domain[index] == root.value):
                    item.scratched[index] = {"type":"ForwardChecking", "value":depth}
            if item.getRemainingValues() == 0:
                return True
        return False

    def getVariable(self, index):
        return self.variables[index[0]][index[1]]

    def undoScratchAtDepth(self, depth, value):
        for i in range(len(self.variables)):
            for j in range(len(self.variables[i])):
                self.variables[i][j].undoScratchAtDepth(depth, value)

    def getMRV(self):
        minimum = float("inf")
        index = None
        for i in range(len(self.variables)):
            for j in range(len(self.variables[i])):
                temp = self.variables[i][j].getRemainingValues()
                # If temp = none, it has value assigned. if temp = 0, its domain is empty.
                if (temp is not None) & (temp < minimum):
                    minimum = temp
                    index = [i, j]

        return index

    def __str__(self):
        horizontal = ""
        for i in range(26):
            horizontal = horizontal + "_"

        grid = ""
        for row in range(len(self.variables)):
            if row%3 == 0:
                grid = grid + horizontal + "\n"
            line = ""
            for column in range(len(self.variables[row])):
                if column%3 == 0:
                    line = line + " |"
                line = line +" "+ str(self.variables[row][column].value)
            grid = grid + line + " |\n"
        return grid + horizontal

    def domainList(self):

        for row in range(len(self.variables)):
            for column in range(len(self.variables[row])):
                print str(row), str(column), "->"
                form = ""
                for i,v in enumerate(self.variables[row][column].scratched):
                    form = form + str(self.variables[row][column].domain[i]) + "-" +str(v)+" "
                print form+"\n"
