# multiAgents.py
# --------------
# Licensing Information:  You are free to use or extend these projects for
# educational purposes provided that (1) you do not distribute or publish
# solutions, (2) you retain this notice, and (3) you provide clear
# attribution to UC Berkeley, including a link to http://ai.berkeley.edu.
#
# Attribution Information: The Pacman AI projects were developed at UC Berkeley.
# The core projects and autograders were primarily created by John DeNero
# (denero@cs.berkeley.edu) and Dan Klein (klein@cs.berkeley.edu).
# Student side autograding was added by Brad Miller, Nick Hay, and
# Pieter Abbeel (pabbeel@cs.berkeley.edu).


from util import manhattanDistance
from game import Directions
import random, util

from game import Agent

class ReflexAgent(Agent):
    """
      A reflex agent chooses an action at each choice point by examining
      its alternatives via a state evaluation function.

      The code below is provided as a guide.  You are welcome to change
      it in any way you see fit, so long as you don't touch our method
      headers.
    """


    def getAction(self, gameState):
        """
        You do not need to change this method, but you're welcome to.

        getAction chooses among the best options according to the evaluation function.

        Just like in the previous project, getAction takes a GameState and returns
        some Directions.X for some X in the set {North, South, West, East, Stop}
        """
        # Collect legal moves and successor states
        legalMoves = gameState.getLegalActions()

        # Choose one of the best actions
        scores = [self.evaluationFunction(gameState, action) for action in legalMoves]
        bestScore = max(scores)
        bestIndices = [index for index in range(len(scores)) if scores[index] == bestScore]
        chosenIndex = random.choice(bestIndices) # Pick randomly among the best

        "Add more of your code here if you want to"

        return legalMoves[chosenIndex]

    def evaluationFunction(self, currentGameState, action):
        """
        Design a better evaluation function here.

        The evaluation function takes in the current and proposed successor
        GameStates (pacman.py) and returns a number, where higher numbers are better.

        The code below extracts some useful information from the state, like the
        remaining food (newFood) and Pacman position after moving (newPos).
        newScaredTimes holds the number of moves that each ghost will remain
        scared because of Pacman having eaten a power pellet.

        Print out these variables to see what you're getting, then combine them
        to create a masterful evaluation function.
        """
        # Useful information you can extract from a GameState (pacman.py)
        successorGameState = currentGameState.generatePacmanSuccessor(action)
        newGhostStates = successorGameState.getGhostStates()
        newFood = successorGameState.getFood()

        newPos = successorGameState.getPacmanPosition()
        newGhostInfo = [[ghostState.getPosition(), ghostState.scaredTimer] for ghostState in newGhostStates]
        foodList = newFood.asList()

        score = successorGameState.getScore()

        foodDistance = []
        foodEvaluation = 0
        for food in foodList:
            foodDistance.append(manhattanDistance(newPos,food))
            foodEvaluation = foodEvaluation + manhattanDistance(newPos,food)

        foodDistance.sort()
        closestFoodEvaluation = 0
        if len(foodDistance) != 0:
            closestFoodEvaluation = 1/float(foodDistance[0])

        ghostsEvaluation = 0
        for ghost in newGhostInfo:
            distance = manhattanDistance(newPos,ghost[0]) + ghost[1]
            ghostsEvaluation = ghostsEvaluation + distance

        #  Needs to be made better. Only got 3/4. Need to get 4/4.
        if ghostsEvaluation > 8:
            return score + closestFoodEvaluation
        else:
            return score + ghostsEvaluation

def scoreEvaluationFunction(currentGameState):
    """
      This default evaluation function just returns the score of the state.
      The score is the same one displayed in the Pacman GUI.

      This evaluation function is meant for use with adversarial search agents
      (not reflex agents).
    """
    return currentGameState.getScore()

class MultiAgentSearchAgent(Agent):
    """
      This class provides some common elements to all of your
      multi-agent searchers.  Any methods defined here will be available
      to the MinimaxPacmanAgent, AlphaBetaPacmanAgent & ExpectimaxPacmanAgent.

      You *do not* need to make any changes here, but you can if you want to
      add functionality to all your adversarial search agents.  Please do not
      remove anything, however.

      Note: this is an abstract class: one that should not be instantiated.  It's
      only partially specified, and designed to be extended.  Agent (game.py)
      is another abstract class.
    """

    def __init__(self, evalFn = 'scoreEvaluationFunction', depth = '2'):
        self.index = 0 # Pacman is always agent index 0
        self.evaluationFunction = util.lookup(evalFn, globals())
        self.depth = int(depth)


class MinimaxAgent(MultiAgentSearchAgent):
    """
      Your minimax agent (question 2)
    """

    def getAction(self, gameState):
        """
          Returns the minimax action from the current gameState using self.depth
          and self.evaluationFunction.

          Here are some method calls that might be useful when implementing minimax.

          gameState.getLegalActions(agentIndex):
            Returns a list of legal actions for an agent
            agentIndex=0 means Pacman, ghosts are >= 1

          gameState.generateSuccessor(agentIndex, action):
            Returns the successor game state after an agent takes an action

          gameState.getNumAgents():
            Returns the total number of agents in the game
        """

        answer = self.stateEvaluation(gameState, 1, self.depth, 0)
        # print answer, self.depth
        return gameState.getLegalActions(0)[answer[1]]

    def stateEvaluation(self, gameState , currentDepth, totalDepth, agentIndex):

        answer = [float("inf"), 0]
        comparer = "min"

        if agentIndex >= gameState.getNumAgents():
            # depth needs to be increased
            currentDepth = currentDepth + 1
            agentIndex = 0

        if currentDepth > totalDepth:
            return [self.evaluationFunction(gameState), 0]

        if agentIndex == 0:
            # Then its pacman
            answer[0] = answer[0] * -1
            comparer = "max"

        agentActions = gameState.getLegalActions(agentIndex)
        # print "length for agent",agentIndex,"is",len(agentActions), "Number of agents", gameState.getNumAgents()
        if len(agentActions) == 0:
            return self.stateEvaluation(gameState, currentDepth, totalDepth, agentIndex+1)

        for i, action in enumerate(agentActions):
            child = gameState.generateSuccessor(agentIndex, action)
            evaluation = self.stateEvaluation(child, currentDepth, totalDepth, agentIndex+1)
            answer = self.minimaxComparer([evaluation[0], i], answer, comparer)

        return answer

    def minimaxComparer(self, newObj, oldObj, comparer):
        if comparer == "max":
            if newObj[0] > oldObj[0]:
                return newObj
            else:
                return oldObj
        else:
            if newObj[0] < oldObj[0]:
                return newObj
            else:
                return oldObj

class AlphaBetaAgent(MultiAgentSearchAgent):
    """
      Your minimax agent with alpha-beta pruning (question 3)
    """

    def getAction(self, gameState):
        """
          Returns the minimax action from the current gameState using self.depth
          and self.evaluationFunction.

          Here are some method calls that might be useful when implementing minimax.

          gameState.getLegalActions(agentIndex):
            Returns a list of legal actions for an agent
            agentIndex=0 means Pacman, ghosts are >= 1

          gameState.generateSuccessor(agentIndex, action):
            Returns the successor game state after an agent takes an action

          gameState.getNumAgents():
            Returns the total number of agents in the game
        """

        answer = self.stateEvaluation(gameState, 1, self.depth, 0, [float("inf")*-1, float("inf")])
        # print answer, self.depth
        return gameState.getLegalActions(0)[answer[1]]

    def stateEvaluation(self, gameState , currentDepth, totalDepth, agentIndex, parentInfo):

        answer = [float("inf"), 0]
        comparer = "min"

        if agentIndex >= gameState.getNumAgents():
            # depth needs to be increased
            currentDepth = currentDepth + 1
            agentIndex = 0

        if currentDepth > totalDepth:
            return [self.evaluationFunction(gameState), 0]

        if agentIndex == 0:
            # Then its pacman
            answer[0] = answer[0] * -1
            comparer = "max"

        selfInfo = parentInfo[:]

        agentActions = gameState.getLegalActions(agentIndex)
        if len(agentActions) == 0:
            return self.stateEvaluation(gameState, currentDepth, totalDepth, agentIndex+1, selfInfo)

        for i, action in enumerate(agentActions):
            child = gameState.generateSuccessor(agentIndex, action)
            evaluation = self.stateEvaluation(child, currentDepth, totalDepth, agentIndex+1, selfInfo)
            answer = self.minimaxComparer([evaluation[0], i], answer, comparer)

            if (comparer == "max") & (answer[0] > selfInfo[1]):
                break
            if (comparer == "min") & (answer[0] < selfInfo[0]):
                break

            if (comparer == "max") & (answer[0] > selfInfo[0]):
                selfInfo[0] = answer[0]

            if (comparer == "min") & (answer[0] < selfInfo[1]):
                selfInfo[1] = answer[0]

        return answer

    def minimaxComparer(self, newObj, oldObj, comparer):
        if comparer == "max":
            if newObj[0] > oldObj[0]:
                return newObj
            else:
                return oldObj
        else:
            if newObj[0] < oldObj[0]:
                return newObj
            else:
                return oldObj

class ExpectimaxAgent(MultiAgentSearchAgent):
    """
      Your expectimax agent (question 4)
    """

    def getAction(self, gameState):
        """
          Returns the expectimax action using self.depth and self.evaluationFunction

          All ghosts should be modeled as choosing uniformly at random from their
          legal moves.
        """

        answer = self.stateEvaluation(gameState, 1, self.depth, 0)
        # print answer, self.depth
        return gameState.getLegalActions(0)[answer[1]]

    def stateEvaluation(self, gameState , currentDepth, totalDepth, agentIndex):

        # For Ghost - answer is average value, N
        # For Pacman - answer is max value, choiceIndex
        answer = [0, 0]
        comparer = "averager"

        if agentIndex >= gameState.getNumAgents():
            # depth needs to be increased
            currentDepth = currentDepth + 1
            agentIndex = 0

        if currentDepth > totalDepth:
            return [self.evaluationFunction(gameState), 0]

        if agentIndex == 0:
            # Then its pacman
            answer[0] = float("inf") * -1
            comparer = "max"

        agentActions = gameState.getLegalActions(agentIndex)
        # print "length for agent",agentIndex,"is",len(agentActions), "Number of agents", gameState.getNumAgents()
        if len(agentActions) == 0:
            return self.stateEvaluation(gameState, currentDepth, totalDepth, agentIndex+1)

        for i, action in enumerate(agentActions):
            child = gameState.generateSuccessor(agentIndex, action)
            evaluation = self.stateEvaluation(child, currentDepth, totalDepth, agentIndex+1)
            answer = self.ExpectimaxComparer([evaluation[0], i], answer, comparer)

        return answer

    def ExpectimaxComparer(self, newObj, oldObj, comparer):
        if comparer == "max":
            if newObj[0] > oldObj[0]:
                return newObj
            else:
                return oldObj
        else:
            newObj[1] = oldObj[1] + 1
            newObj[0] = (oldObj[0] * oldObj[1] + newObj[0])/float(newObj[1])
            return  newObj


def betterEvaluationFunction(currentGameState):
    """
      Your extreme ghost-hunting, pellet-nabbing, food-gobbling, unstoppable
      evaluation function (question 5).

      DESCRIPTION: <write something here so we know what you did>
        https://youtu.be/ZYlARRvv8J8?t=1m11s
        Vathi kuchi iduppa than aati
        Nenjukulla aduppa than mooti
        Ayyo Amma enna iva vaati vathaikeera...
    """
    newGhostStates = currentGameState.getGhostStates()
    newFood = currentGameState.getFood()

    newPos = currentGameState.getPacmanPosition()
    newGhostInfo = [[ghostState.getPosition(), ghostState.scaredTimer] for ghostState in newGhostStates]
    foodList = newFood.asList()

    score = currentGameState.getScore()

    foodDistance = []
    foodEvaluation = 0
    for food in foodList:
        foodDistance.append(manhattanDistance(newPos,food))
        foodEvaluation = foodEvaluation + manhattanDistance(newPos,food)

    foodDistance.sort()
    closestFoodEvaluation = 0
    if len(foodDistance) != 0:
        closestFoodEvaluation = 1/float(foodDistance[0])

    ghostsEvaluation = 0
    for ghost in newGhostInfo:
        distance = manhattanDistance(newPos,ghost[0]) + ghost[1]
        ghostsEvaluation = ghostsEvaluation + distance

    #  Needs to be made better. Only got 3/4. Need to get 4/4.
    if ghostsEvaluation > 8:
        return score + closestFoodEvaluation
    else:
        return score + ghostsEvaluation

# Abbreviation
better = betterEvaluationFunction
